import os
import re
import errno
# use google's sentencepiece to build bep encoding
import sentencepiece as spm
import tensorflow as tf
import logging


class PrePro(object):
    def __init__(self, config):
        self.en_blank_pattern = re.compile(r'[0-9a-zA-z][,.!?\":;()%&*]|'
                                           r'[,.!?\":;()%&*][0-9a-zA-z]|'
                                           r'[,.!?\":;()%&*][,.!?\":;()%&*]')
        self.data_path = config.data_path
        self.prepro_path = config.prepro_path
        self.cn_vocab_size = config.cn_vocab_size
        self.en_vocab_size = config.en_vocab_size

    def _read_file(self, file_path):
        cn_samples = []
        en_samples = []
        meta_pattern = re.compile(r'^<url>|^<keywords>|^<speaker>|'
                                  r'^<talkid>|^<title>|^<description>|'
                                  r'^<reviewer|^<translator')
        cn_pattern = re.compile(r'(（鼓掌）|（众人笑）|（众人鼓掌）)')

        with open(file_path + 'train.tags.en-zh.en', 'r') as f_en, \
                open(file_path + 'train.tags.en-zh.zh', 'r') as f_cn:

            for en_sentence in f_en:
                en_sample = en_sentence.strip().lower()
                cn_sample = f_cn.readline().strip()

                if meta_pattern.search(en_sample) is None:
                    cn_sample = cn_pattern.sub('', cn_sample)
                    cn_sample = re.sub(r'\s+', ' ', cn_sample)

                    if len(en_sample) > 1 and len(cn_sample) > 0:

                        while True:
                            if self.en_blank_pattern.search(en_sample) is not None:
                                joint_object = self.en_blank_pattern.search(en_sample).span()
                                s_list = list(en_sample)[:joint_object[0] + 1] + [' '] + list(en_sample)[
                                                                                         joint_object[-1] - 1:]
                                en_sample = ''.join(s_list)
                            else:
                                break
                        en_samples.append(en_sample)

                        cn_samples.append(cn_sample)

        assert len(cn_samples) == len(en_samples)
        return en_samples, cn_samples

    @staticmethod
    def write(sents, fname):
        with open(fname, 'w') as fout:
            fout.write("\n".join(sents))

    @staticmethod
    def segment_and_write(sp, sents, fname):
        with open(fname, "w") as fout:
            for sent in sents:
                pieces = sp.EncodeAsPieces(sent)
                fout.write(" ".join(pieces) + "\n")

    def run(self):
        """Load raw data -> Pre-processing -> Segmenting with sentencepice
            config: configurations
        """
        logging.basicConfig(level=logging.INFO)
        logging.info("# Check if raw files exist")
        train_en = self.data_path + "train.tags.en-zh.en"
        train_cn = self.data_path + "train.tags.en-zh.zh"
        for f in (train_en, train_cn):
            if not os.path.isfile(f):
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), f)

        logging.info("# Preprocessing")
        prepro_train_en, prepro_train_cn = self._read_file(self.data_path)

        logging.info("Let's see how preprocessed data look like")
        logging.info("prepro_train_en:", prepro_train_en[0])
        logging.info("prepro_train_cn:", prepro_train_cn[0])

        logging.info("# write preprocessed files to disk")
        os.makedirs(self.data_path + "prepro", exist_ok=True)

        self.write(prepro_train_en, self.data_path + "prepro/train.en")
        self.write(prepro_train_cn, self.data_path + "prepro/train.cn")

        logging.info("# Train two BPE model with sentencepiece")
        os.makedirs(self.prepro_path, exist_ok=True)
        # --model_prefix: output model name prefix
        en_train = '--input={} --pad_id=0 --unk_id=1 ' \
                   '--bos_id=2 --eos_id=3 --model_prefix={} ' \
                   '--vocab_size={} --model_type=bpe'.format(self.data_path + "prepro/train.en",
                                                             self.prepro_path + "en_bpe",
                                                             self.en_vocab_size)
        cn_train = '--input={} --pad_id=0 --unk_id=1 ' \
                   '--bos_id=2 --eos_id=3 --model_prefix={} ' \
                   '--vocab_size={} --model_type=bpe'.format(self.data_path + "prepro/train.cn",
                                                             self.prepro_path + "cn_bpe",
                                                             self.cn_vocab_size)
        spm.SentencePieceTrainer.Train(en_train)
        spm.SentencePieceTrainer.Train(cn_train)

        logging.info("# Load trained bpe model")

        en_sp = spm.SentencePieceProcessor()
        en_sp.Load(self.prepro_path + "en_bpe.model")

        cn_sp = spm.SentencePieceProcessor()
        cn_sp.Load(self.prepro_path + "cn_bpe.model")

        logging.info("# Segment")

        # split 4000 sentences for validation
        split_train_en = prepro_train_en[:-4000]
        split_train_cn = prepro_train_cn[:-4000]
        split_valid_en = prepro_train_en[-4000:]
        split_valid_cn = prepro_train_cn[-4000:]

        self.segment_and_write(en_sp, split_train_en, self.prepro_path + "train.en.bpe")
        self.segment_and_write(cn_sp, split_train_cn, self.prepro_path + "train.cn.bpe")
        self.segment_and_write(en_sp, split_valid_en, self.prepro_path + "valid.en.bpe")
        self.segment_and_write(cn_sp, split_valid_cn, self.prepro_path + "valid.cn.bpe")
        # used for calculating BLEU score
        split_valid_cn = [i for i in split_valid_cn]
        self.write(split_valid_cn, self.data_path + "prepro/valid.cn")

        logging.info("Let's see how segmented data look like")
        print("train_en:", open(self.prepro_path + "train.en.bpe", 'r').readline())
        print("train_cn:", open(self.prepro_path + "train.cn.bpe", 'r').readline())


class DataLoader(object):
    def __init__(self, config):
        self.prepro_path = config.prepro_path
        self.data_path = config.data_path
        self.max_len = config.max_len
        self.buffer_size = config.buffer_size
        self.batch_size = config.batch_size

    def load_data(self, mode):
        '''Loads source and target data and filters out too lengthy samples.
        sents_en: list of source sents
        sents_cn: list of target sents
        '''
        if mode == 'train':
            en_dataset = tf.data.TextLineDataset(self.prepro_path + 'train.en.bpe', num_parallel_reads=4)
            cn_dataset = tf.data.TextLineDataset(self.prepro_path + 'train.cn.bpe', num_parallel_reads=4)
            dataset = tf.data.Dataset.zip((en_dataset, cn_dataset))
        elif mode == 'valid':
            en_dataset = tf.data.TextLineDataset(self.prepro_path + 'valid.en.bpe', num_parallel_reads=4)
            cn_dataset = tf.data.TextLineDataset(self.prepro_path + 'valid.cn.bpe', num_parallel_reads=4)
            dataset = tf.data.Dataset.zip((en_dataset, cn_dataset))
        else:
            print('can not load data %s' % mode)
            quit()
        return dataset

    @staticmethod
    def load_vocab(prepro_path, mode='en'):
        '''Loads vocabulary file and returns idx<->token maps
        Note that these are reserved
        0: <pad>, 1: <unk>, 2: <s>, 3: </s>

        Returns
        two dictionaries.
        '''
        if mode == 'en':
            en_vocab = [line.split()[0] for line in open(prepro_path + 'en_bpe.vocab', 'r').read().splitlines()]
            en_token2idx = {token: idx for idx, token in enumerate(en_vocab)}
            en_idx2token = {idx: token for idx, token in enumerate(en_vocab)}
            return en_token2idx, en_idx2token
        elif mode == 'cn':
            cn_vocab = [line.split()[0] for line in open(prepro_path + 'cn_bpe.vocab', 'r').read().splitlines()]
            cn_token2idx = {token: idx for idx, token in enumerate(cn_vocab)}
            cn_idx2token = {idx: token for idx, token in enumerate(cn_vocab)}
            return cn_token2idx, cn_idx2token
        else:
            print('vocabulary only exist for english and chinese')
            quit()

    def encode(self, en_sent, cn_sent):
        en_token2id, _ = self.load_vocab(self.prepro_path, 'en')
        cn_token2id, _ = self.load_vocab(self.prepro_path, 'cn')

        en_id = [en_token2id.get(tok, en_token2id['<unk>']) for tok in en_sent.numpy().decode("utf-8").split() + ["</s>"]]
        cn_id = [cn_token2id.get(tok, cn_token2id['<unk>']) for tok in ["<s>"] + cn_sent.numpy().decode("utf-8").split() + ["</s>"]]

        return en_id, cn_id

    def tf_encode(self, en, cn):
        result_en, result_cn = tf.py_function(self.encode, [en, cn], [tf.int32, tf.int32])
        result_en.set_shape([None])
        result_cn.set_shape([None])

        return result_en, result_cn

    def filter_max_length(self, x, y):
        return tf.logical_and(tf.size(x) <= self.max_len,
                              tf.size(y) <= self.max_len)

    def dataset_producer(self):
        train_examples = self.load_data('train')
        val_examples = self.load_data('valid')

        train_preprocessed = (
            train_examples
                .map(self.tf_encode)
                .filter(self.filter_max_length)
                # cache the dataset to memory to get a speedup while reading from it.
                .cache()
                .shuffle(self.buffer_size))

        val_preprocessed = (
            val_examples
                .map(self.tf_encode)
                .filter(self.filter_max_length))

        train_dataset = (train_preprocessed
                         .padded_batch(self.batch_size, padded_shapes=([None], [None]))
                         .prefetch(tf.data.experimental.AUTOTUNE))

        val_dataset = (val_preprocessed
                       .padded_batch(self.batch_size, padded_shapes=([None], [None])))

        return train_dataset, val_dataset

import time
import tensorflow as tf
from transformer import Transformer
from load_data import DataLoader


class Config(object):
    # prepro
    en_vocab_size = 10000
    cn_vocab_size = 10000

    # preprocessed data path
    project_path = '/content/drive/My Drive/CN_NMT/'
    prepro_path = project_path + 'data/segmented/'
    data_path = project_path + 'data/'

    # training scheme
    batch_size = 64
    eval_batch_size = 32
    warmup_steps = 4000
    buffer_size = 7000
    num_epochs = 40
    eval_dir = project_path + "/eval/"

    # model setting
    d_model = 256
    d_feedforward = 1024
    num_blocks = 6
    num_heads = 8
    max_len = 84
    dropout_rate = 0.1

    # test setting
    ckpt_dir = project_path + '/checkpoint/'


class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, d_model, warmup_steps=4000):
        super(CustomSchedule, self).__init__()

        self.d_model = d_model
        self.d_model = tf.cast(self.d_model, tf.float32)

        self.warmup_steps = warmup_steps

    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)


class RunModel(object):
    def __init__(self, config):
        self.config = config
        self.transformer = Transformer(config)

        self.learning_rate = CustomSchedule(config.d_model)
        self.optimizer = tf.keras.optimizers.Adam(self.learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)

        self.loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')
        self.train_loss = tf.keras.metrics.Mean(name='train_loss')
        # TODO: change accuracy to valid BLEU Score
        self.train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

    def loss_function(self, real, pred):
        mask = tf.math.logical_not(tf.math.equal(real, 0))
        loss_ = self.loss_object(real, pred)

        mask = tf.cast(mask, dtype=loss_.dtype)
        loss_ *= mask

        return tf.reduce_mean(loss_)

    @tf.function(input_signature=[tf.TensorSpec(shape=(None, None), dtype=tf.int32),
                                  tf.TensorSpec(shape=(None, None), dtype=tf.int32)])
    def train_step(self, encoder_input, target_seq):
        """
        :param encoder_input: [batch_size, max en_seq_len]
                one_example: [tok_1, ..., tok_n, </s>, <pad>, <pad>...]
        :param target_seq: [batch_size, max_cn_seq_len]
                one_example: [<s>, tok_1, ..., tok_n, </s>, <pad>, ...]
        """
        decoder_input = target_seq[:, :-1]
        # TODO: decoder_input include <s> and </s>, only the longest sentences do not include </s>
        tar_real = target_seq[:, 1:]

        with tf.GradientTape() as tape:
            predictions, _ = self.transformer(encoder_input, decoder_input, True)
            loss = self.loss_function(tar_real, predictions)

        gradients = tape.gradient(loss, self.transformer.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.transformer.trainable_variables))

        self.train_loss(loss)
        self.train_accuracy(tar_real, predictions)

    def training(self):
        loader = DataLoader(config)
        train_dataset, val_dataset = loader.dataset_producer()

        ckpt = tf.train.Checkpoint(transformer=self.transformer,
                                   optimizer=self.optimizer)
        ckpt_manager = tf.train.CheckpointManager(ckpt, config.ckpt_dir, max_to_keep=2)

        # if a checkpoint exists, restore the latest checkpoint.
        if ckpt_manager.latest_checkpoint:
          ckpt.restore(ckpt_manager.latest_checkpoint)
          print ('Latest checkpoint restored!')

        for epoch in range(config.num_epochs):
            start = time.time()

            self.train_loss.reset_states()
            self.train_accuracy.reset_states()

            # inp -> english, tar -> chinese
            for (batch, (inp, tar)) in enumerate(train_dataset):
                self.train_step(inp, tar)

                if batch % 50 == 0:
                    print(f'Epoch {epoch + 1}, Batch {batch}, Loss {self.train_loss.result():.4f}, '
                          f'Accuracy {self.train_accuracy.result():.4f}, Running Time {(time.time() - start):.2f}s')
                    start = time.time()

            if (epoch + 1) % 2 == 0:
                ckpt_save_path = ckpt_manager.save()
                print(f'Saving checkpoint for epoch {epoch + 1} at {ckpt_save_path}')

            print(f'Epoch {epoch + 1} Loss {self.train_loss.result():.4f} Accuracy {self.train_accuracy.result():.4f}')


if __name__ == '__main__':
    config = Config()
    run_model = RunModel(config)
    run_model.training()